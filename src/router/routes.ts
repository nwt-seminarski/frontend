export enum PageRoute {
  LOGIN = '/login',
  SIGNUP = '/signup',
  CHAT = '/chat',
  HOME = '/home',
}
