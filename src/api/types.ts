export interface ApiLoginBody {
  username: string;
  password: string;
}

export interface ApiLoginResponse {
  access_token: string;
  username: string;
  userId: number;
}

export interface ApiSignupBody {
  username: string;
  password: string;
}

export interface ApiSignupResponse {
  access_token: string;
  username: string;
  userId: number;
}
