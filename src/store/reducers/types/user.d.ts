import {
  ActiveUsersUser,
  AllUsersGetUserWsMessage,
  InboxGetMessageWsMessage,
  LoadMessagesMessageWsMessage,
} from '../../../ws/constants';

export interface UserState {
  isLoggedIn: boolean;
  token: string | null;
  username: string | null;
  userId: number | null;
  activeUsers: ActiveUsersUser[];
  allUsers: AllUsersGetUserWsMessage[];
  wsConnected: boolean;
}

export interface ChatState {
  inbox: InboxGetMessageWsMessage[];
  messages: LoadMessagesMessageWsMessage[];
  limit: number;
  offset: number;
  chatWithUserId: number | null;
}
