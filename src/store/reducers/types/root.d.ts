import { ChatState, UserState } from './user';

export interface RootState {
  user: UserState;
  chat: ChatState;
}
